"""
    Developed by Knox Cavitt and Bhaskar Gaur
    Connection handler used by the MITM.

    This this should actually be the fsm

"""

import ssl
import socket
import os
import sys
import select
import subprocess


from pprint import pprint
from time import sleep

ssl.HAS_SNI = True
context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.set_ciphers(
    "TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:DHE-RSA-AES256-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:RSA-PSK-AES256-GCM-SHA384:DHE-PSK-AES256-GCM-SHA384:RSA-PSK-CHACHA20-POLY1305:DHE-PSK-CHACHA20-POLY1305:ECDHE-PSK-CHACHA20-POLY1305:AES256-GCM-SHA384:PSK-AES256-GCM-SHA384:PSK-CHACHA20-POLY1305:RSA-PSK-AES128-GCM-SHA256:DHE-PSK-AES128-GCM-SHA256:AES128-GCM-SHA256:PSK-AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:ECDHE-PSK-AES256-CBC-SHA384:ECDHE-PSK-AES256-CBC-SHA:SRP-RSA-AES-256-CBC-SHA:SRP-AES-256-CBC-SHA:RSA-PSK-AES256-CBC-SHA384:DHE-PSK-AES256-CBC-SHA384:RSA-PSK-AES256-CBC-SHA:DHE-PSK-AES256-CBC-SHA:AES256-SHA:PSK-AES256-CBC-SHA384:PSK-AES256-CBC-SHA:ECDHE-PSK-AES128-CBC-SHA256:ECDHE-PSK-AES128-CBC-SHA:SRP-RSA-AES-128-CBC-SHA:SRP-AES-128-CBC-SHA:RSA-PSK-AES128-CBC-SHA256:DHE-PSK-AES128-CBC-SHA256:RSA-PSK-AES128-CBC-SHA:DHE-PSK-AES128-CBC-SHA:AES128-SHA:PSK-AES128-CBC-SHA256:PSK-AES128-CBC-SHA"
)
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE


def handler(conn, addr, child_conn, parent_conn):
    server_ip = "127.0.0.1"
    server_port = "8082"

    server_ip = "160.36.239.51"
    server_port = "443"

    '''

    CLIENT_TLS = True # Shows if the client supports TLS - need to get it from the client
    TLS_AVAILABLE = True # Same as above but server and the code it written below
    try:
        with socket.create_connection((server_ip, 443)) as sock:
            with context.wrap_socket(sock, server_hostname=server_ip) as ssock:
                ssock.version()

                pprint(ssl.get_server_certificate((server_ip, server_port)))
        
    except ssl.SSLError as e:
        if e.reason == 'WRONG_VERSION_NUMBER':
            TLS_AVAILABLE = False
        else:
            # If there is an error we dont catch here we should drop it.
            print(e)

    # Certificate checking
    if TLS_AVAILABLE:
        print('idk')

    # Means we dont need to do anything and can let the client and server communicate freely
    # Need to write code to step out of the way but for rn just being a proxy would work.
    # Becuase of the above line I will print then flip CLIENT_TLS
    if TLS_AVAILABLE and CLIENT_TLS:
        print("Exiting from thread - no addition action needed.")
        CLIENT_TLS = False
        #sys.exit(0)
    '''
    # Means we can proxy here for http->https

    #if TLS_AVAILABLE and not CLIENT_TLS:
    if True:
        print('hit')
        '''
        # Generate the key to send back so that when we are done we can remove the thread and connection
        key = addr[0] + str(addr[1])
        print(key)
        conn_key = hashlib.sha256(key.encode()).hexdigest()
        conn_key += str(parent_conn)
        print("connection")
        '''


        #print(' -L -p tcp -s {0} --sport {1}'.format(addr[0], addr[1]))
        #r = subprocess.run(['conntrack', '-L -p tcp -s {0} --sport {1}'.format(addr[0], addr[1])], stdout=subprocess.PIPE)
        #pprint(r.stdout) 

        #r = subprocess.run(['conntrack', '-L'], stdout=subprocess.PIPE)
        #t = str(r.stdout)
        #print(t.find('192.168.1.128').fin)
        #print(addr)
        #r = subprocess.Popen(['conntrack', '-L'], stdout=subprocess.PIPE)
        #q = subprocess.check_output(['grep', addr[0]], stdin=r.stdout)

        #x = subprocess.check_output(['grep', str(addr[1])], stdin=subprocess.PIPE)
        #x.communicate(input=q)[0]

        #print(x.decode())
        
        #print(str(q).find(str(addr[1])))

        #print(str(q).split('tcp'))

        #x = subprocess.run(['grep', '60820'], stdin=q.stdout, stdout=subprocess.PIPE)

 
 
        #nat_translation = os.popen('sudo conntrack -L | grep 192.168.1.128 | grep 60820').read()

        #print(nat_translation)



        # Explictly set the port to 443 for https###############
        with socket.create_connection((server_ip, server_port)) as sock:
          with context.wrap_socket(sock, server_hostname=server_ip) as ssock:
                
                '''
                active_descriptors = [ssock, conn]
                
                try:
                    while True:

                        rl, _, _ = select.select(active_descriptors, [], [])
                        for t in rl:

                            fin = t.recv(1024)
                            print(fin)

                            if t == conn:
                                print('SENDING FROM: ', t)
                                ssock.send(fin)
                            else:
                                
                                print('SENDING FROM: ', t)
                                conn.send(fin)

                
                            
                except KeyboardInterrupt:
                    print("Exiting from thread.")
                    #os.system('sudo conntrack -F') # For testing only!
                    sys.exit(0)
                '''
handler(None, None, None, None)

