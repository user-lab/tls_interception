import re
import json
import os
import random, string
from subprocess import Popen, PIPE

# Used for extracing certificate from packet stream
def grab_cert(packet_record, curr_packet) -> int:
    
    raw_load = curr_packet
    kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
    print(kind_load, "\n")
    # If we have completed the cert there is no need to anything
    if packet_record['Is_Complete']:
        return 1
    # If the cert is incomplete, then 
    elif packet_record['Len_of_cert'] == 0:
        search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load) #Search for Cert
        #print(kind_load, "\n")
        #print(search)

        # Regex returns a none type if the pattern isnt found
        if search != None:
            
            print('Begin collecting certificate.')
            range = search.span()

            # Printing the size of the key and calculate the length as chars
            cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]	
            cert_size_chars = int(cert_size_hex_string.replace(' ', ''), 16) * 3	
            

            # Inititlizing the certificate data			
            packet_record['Len_of_cert'] = cert_size_chars
            packet_record['Cert_Char_Remaining'] = cert_size_chars - len(kind_load[range[0]+15:]) 
            packet_record['Complete_Cert'] += kind_load[range[0]+15:]
            #print(json.dumps(packet_record, indent=4, sort_keys=True))

    elif packet_record['Len_of_cert'] > 0:
        search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load) #Search for Cert
        #print(kind_load, "\n")
        #print(search)

        print('Collecting certificate for ', packet_record['Cert_Char_Remaining'], ' remaining char.')

        # If I have enough room in my remaining chars to add the whole of the packet then I do
        if len(kind_load) <= packet_record['Cert_Char_Remaining']:
            packet_record['Complete_Cert'] += kind_load
            packet_record['Cert_Char_Remaining'] -=len(kind_load)

            if packet_record['Cert_Char_Remaining'] <= int(0):
                packet_record['Cert_Char_Remaining'] = 0
                packet_record['Is_Complete'] = True
                print('Finished collecting Certificate.')
                packet_record['pem'] = write_cert(packet_record['Complete_Cert'])
                return 1

        else:
            packet_record['Complete_Cert'] += kind_load[:packet_record['Cert_Char_Remaining']]
            packet_record['Cert_Char_Remaining'] = 0
            packet_record['Is_Complete'] = True
            print('Finished collecting Certificate.')
            packet_record['pem'] = write_cert(packet_record['Complete_Cert'])
            return 1

    return 0

def write_cert(certificate_data):
    # stores the hex string in a file
    crt = certificate_data[30:-2]
    fname = randomword(10)
    
    txt = fname + '.txt'
    der = fname + '.der'
    pem = fname + '.pem'
    
    fout = open(txt, 'w')
    fout.write(crt)
    fout.close()

    # converts hex string to a DER and a PEM
    os.system("xxd -r -ps " + txt + " " + der +";"+ "openssl x509 -in " + der + " " + "-inform der -out " + pem)
    os.system("rm -f " + txt + " " + der)
    print("generated certificate")
    return pem
    
    #openssl = Popen([ 'openssl x509 -in', der, '-inform der', '-out', pem])
    #xxd = Popen([ 'xxd -r -ps', txt, der])
    #openssl.communicate()
    #xxd.wait()
    #print('exit status: ', openssl.returncode, " ", xxd.returncode)
    '''
    cmd = Popen([ 'xxd -r -ps | openssl x509 -inform der'], shell=True, stdout=PIPE)
    cmd.communicate(crt)
    output = cmd.communicate()[0]
    error = cmd.communicate()[1]
    print("output is: ", output)
    print("error is: ", error)
    '''


def write_cert1(certificate_data):
    # stores the hex string in a file
    crt = certificate_data[30:-2]
    print("crt data:\n", crt, "\n")
    #openssl = Popen(['openssl x509 -inform der'], shell=True, stdin=PIPE, stdout=PIPE)
    #xxd = Popen(['xxd -r -ps'], shell=True, stdin=PIPE, stdout=openssl.stdin)
    '''
    xxd = Popen(['xxd -r -ps'], shell=True, stdin=PIPE, stdout=PIPE)
    echo = Popen(['echo', crt], shell=True, stdout=xxd.stdin)

    output = xxd.communicate()[0]
    statuses = [echo.wait(), xxd.returncode]

    print("Certificate in PEM format: \n", output, "\n")
    '''
    cmd = Popen([ 'echo', crt, ' | xxd -r -ps'], shell=True, stdout=PIPE)
    output = cmd.communicate()[0]
    statuses = [cmd.wait(), cmd.returncode]
    print("Certificate in PEM format: \n", output, "\n")
    #Not sure if using 3 different Popen is better latency wise than making a temp file running a single cmd

def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

def test_tls(args):
    ...
