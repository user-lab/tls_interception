# Nat is a dictionary that is shared anmoungst processes and is updated here. It holds connection info.
# Snapshot is the previous read of the log files as an array of strings, only has iptale entries
def update(nat, snapshot, logname):

    # Might need to move to a seek instead of a readlines
    with open(logname, "r") as fin:
        out = fin.readlines()

        new_ss = []
        for line in out:
            if "iptables_rule " in line:
                idx = line.index("iptables_rule ")
                new_ss.append(line[idx + 16 :].split(" "))

    # No changes to be made
    if snapshot[-1] == new_ss[0]:
        return -1

    # Only check the new entries
    indx = new_ss.index(snapshot[-1])
    new_ss = new_ss[indx:]

    # Update the dict - key is source:sport
    for entry in new_ss:
        src = (entry[3].split("="))[1]
        sport = (entry[12].split("="))[1]

        key = src + ":" + sport
        if key not in nat.keys():
            nat[key] = entry
