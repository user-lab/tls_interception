"""
    Developed by Knox Cavitt and Bhaskar Gaur
    MITM - recieves a connection and spins up a thread to handle it,
    thread reports back when its done to remove it
    from the appropriate structures

"""

import socket
import os
import sys


from datetime import datetime
from multiprocessing import Process, Manager
from connection_handle import handler
from nat_update import update
from loguru import logger
from time import sleep


def main():
    # Will need to be removed when actually running.
    # This is used to create a new log every run.
    try:
        os.remove("mitm.log")
    except OSError:
        pass

    # Add the log file to loguru
    logger.add("mitm.log")

    log_name = "/var/log/syslog"
    # Create a snapshot of the log file
    with open(log_name) as fin:
        out = fin.readlines()

        ip_lines = []
        for line in out:
            if "iptables_rule " in line:
                idx = line.index("iptables_rule ")
                ip_lines.append(line[idx + 16:].split(" "))

    # Puts the snapshot into a shared dict to pass as needed
    nat = Manager()
    nat = nat.dict()
    
    for l in ip_lines:

        src = (l[3].split("="))[1]
        sport = (l[12].split("="))[1]

        key = src + ":" + sport

        if key not in nat.keys():
            nat[key] = l

    # Main loop to spin up threads for the connections
    port = 9006
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:

        # Lets the socket be reused
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Server setup and start of log
        sock.bind(("192.168.4.1", port))
        sock.listen(5)
        logger.debug("MITM is up and listening on 192.168.4.1:%d" % port)

        num_connections = 0

        try:
            while True:

                conn, addr = sock.accept()

                num_connections += 1
                logger.debug("Connection recieved from %s on port %d" % addr)

                # Updates the nat table
                logger.debug("Updating NAT")
                
                p = Process(target=update, args=(nat, ip_lines, log_name,))
                p.start()
                p.join()
                

                # Creates the process to handle incoming connections
                p = Process(target=handler, args=(conn, addr, nat, logger,))
                p.start()

                # Logging info
                curr_time = datetime.now()
                logger.debug("Handler Started at {0}".format(curr_time))

        except KeyboardInterrupt:
            logger.debug("MITM KILLED")
            logger.debug("Total number of connections %d" % num_connections)
            sys.exit(1)


if __name__ == "__main__":
    main()
