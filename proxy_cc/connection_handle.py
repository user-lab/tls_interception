"""
    Developed by Knox Cavitt and Bhaskar Gaur
    Connection handler used by the MITM.

"""
import re
import json
import socket
import sys
import select
import yaml
import multiprocessing as mp

from statemachine import grab_cert
from valid_functions import * # functions used for validation
from yaml.loader import SafeLoader
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from multiprocessing.pool import ThreadPool

# Gets the dst_ip and dport and return it
def extract_dst(nat_entry):
    return (nat_entry[4].split("="))[1], (nat_entry[13].split("="))[1]

# Handles the connection
def handler(conn, addr, nat, logger):
    logger.debug(
        "{0}:{1} ~ Beginning connection handle thread.".format(addr[0], addr[1])
    )

    # Grab the dst data
    L = nat["{0}:{1}".format(addr[0], addr[1])]
    server_ip, server_port = extract_dst(L)

    # Connect to the server
    logger.debug(
        "{0}:{1} ~ Connecting to {2}:{3}` ".format(
            addr[0], addr[1], server_ip, server_port
        )
    )

    universal_args = []
    client_hello = ''
    curr_dns = ''
    server_hello = ''

    # if attempt to connect to server via http:
    #   see if we can upgrade:
    #       if can upgrade: do it
    #       else: drop
    # if https->https:
    #   see if cert valid:
    #       if not: renegotiate
    #       else: transparent proxy (the code below)
    # if http->https:
    #   proxy it

    with socket.create_connection((server_ip, server_port)) as sock:
        active_descriptors = [sock, conn]

        record = {
            "Complete_Cert": "",
            "Len_of_cert": 0,
            "Cert_Char_Remaining": 99999999999999999999,
            "Is_Complete": False,
            "pem": ""
        }

        try:

            # Flags for fsm
            record_cert = 0
            break_connection = False

            # Sending data back and forth as needed
            while True:

                rl, _, _ = select.select(active_descriptors, [], [])
                for t in rl:

                    fin = t.recv(1024)

                    if fin == b"":
                        break

                    print(fin, "\n")


                    # Sends the data
                    if t == conn:
                        sock.send(fin)
                        
                        # grabbing the client hello for subject name checking
                        kind_load = ' '.join('{:02x}'.format(x) for x in fin)
                        search = re.search('16 03 01', kind_load)
                        if search != None:
                            client_hello = kind_load
                            print("Client hello is: ", client_hello)

                    else:
                        conn.send(fin)

                        if server_hello == "":
                            # grabbing the client hello for subject name checking
                            kind_load = ' '.join('{:02x}'.format(x) for x in fin)
                            search = re.search('16 03 03 .. .. 02', kind_load)
                            if search != None:
                                # grabing the range of the match
                                range = search.span()
                                # printing the size of the key and calculate the length as chars
                                cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]
                                print("Server Hello size: hex-{0} decimal-{1}".format(cert_size_hex_string, int(cert_size_hex_string.replace(' ', ''), 16)))
                                #add 5 as the size starts after first 5 char
                                cert_size_chars = (int(cert_size_hex_string.replace(' ', ''), 16) + 5) * 3
                                server_hello = kind_load[int(range[0]):cert_size_chars]
                                print("Server hello is: ", server_hello)

                        #if cert is not yet fully compiled
                        if not record_cert:
                            record_cert = grab_cert(record, fin)
                            #print(json.dumps(record, indent=4, sort_keys=True))
                            print("Is certificate done: ", record['Is_Complete'], " ", record_cert)

                        if record_cert:
                            #ALERT !! This condition might not hit with wget as it ends when certificate is collected.
                            print("pem file: \n", record['pem'])

                            # loads in the PEM using the cryptography library
                            fin = open(record['pem'], 'rb')
                            loaded_cert = x509.load_pem_x509_certificate(fin.read(), default_backend())
                            fin.close()

                            universal_args.append(loaded_cert)
                            universal_args.append(client_hello)
                            universal_args.append(curr_dns)
                            universal_args.append(server_hello)
                            universal_args.append(record)

                            results = 0 #CHANGE ME TO -1 TO MAKE CERTS INVALID
                            config_options = None

                            # load validation options
                            with open('config.yaml') as conf:
                                config_options = yaml.load(conf, Loader=SafeLoader)

                            global SUM
                            SUM = 0
                                    # validate
                            pp = ThreadPool(mp.cpu_count())
                            r = [pp.apply_async(valid_functions[option], args=(universal_args,)) for option in config_options['ValidationOptions']]

                            sleep(1)
                            [add_to_sum(int(R.get())) for R in r]

                            pp.close()
                            pp.join()

                            # make sure every check returned valid
                            if len(config_options['ValidationOptions']) != int(SUM):
                                print("INVALID!")
                                print("The certificate is invalid.")
                                break_connection = True
                                conn.close()
                                sock.close()
                                break
                                #add_to_banned(loaded_cert, banned_certs)

                            else:
                                print('VALID!!')
                                #add_to_captured(loaded_cert, captured_certs)

                            dump_cert(universal_args)
                            # Cert check
                            # Would be another thread?

                if break_connection:
                    print("The connection is being severed.")
                    break

                            

        except KeyboardInterrupt:
            print("Exiting from thread.")
            sys.exit(0)

# used in validation checks
def add_to_sum(num):
    global SUM
    SUM += num