'''
	Functions used to validate a x509 certificate via cryptography library
'''


from cryptography.hazmat.primitives import hashes
from datetime import datetime
from cryptography.x509.oid import NameOID
from cryptography.x509 import NameAttribute
from time import sleep
from ciphers import cipher_dict

import os


def self_signed(args) -> int:
	
	cert = None
	cert = args[0]
	
	if cert.issuer == cert.subject:
		return 0

	print('Passed self signed check')
	return 1

# Checks the valid date
def valid_date(args) -> int:
	cert = None
	cert = args[0]

	print('CHECKING DATE')
	
	if cert.not_valid_before > datetime.now(): return 0
	if cert.not_valid_after < datetime.now(): return 0

	print('Passed valid date')
	return 1

# Checks the CN from the client hello with the CN of the certificate
def valid_subject(args) -> int:
	cert = args[0] 
	client_hello = args[1]
	
	print('CHECKING SUBJECT NAME')
	
	subject = cert.subject 
	

	# Gets all of the data 
	subject_data = {}
	
	for entry in subject:
		data = entry.rfc4514_string()
		data = data.split('=')
		
		# Data[2.5.4.17] is the zipcode, turns out it doesnt have an rc string		
		subject_data[data[0]] = data[1]
	

	# Claculates all of the offset to client_hello ends up being just the extensions
	client_hello = client_hello[3 * 43:]
	len_of_sessionid = client_hello[0:2]
	i_len_of_sessionid = int(len_of_sessionid.replace(' ', ''), 16)
	client_hello = client_hello[(i_len_of_sessionid+1) * 3:]
	i_len_of_suite = int(client_hello[0:5].replace(' ', ''), 16)
	client_hello = client_hello[(i_len_of_suite + 2) * 3:]

	# Piece togther the hex string 
	to_test = ''.join(chr(int(i, 16)) for i in client_hello.split())

	cn = subject_data["CN"]
	
	# Removes the www option
	if cn.find('www') >= 0:
		cn = cn.replace('www.', '')
	elif cn.find('*.') >= 0:
		cn = cn.replace('*.', '')

	
	# Does the test to see if the host name matches
	if to_test.find(cn) >= 0: 
		print('Passed SNI checking')
		return 1
	

	return 0


def valid_subject_dns(args) -> int:
	cert = args[0]
	dns_summary = args[2]

	subject = cert.subject 
	
	print('CHECKING SUBJECT NAME VIA DNS')

	# Gets all of the data 
	subject_data = {}
	
	for entry in subject:
		data = entry.rfc4514_string()
		data = data.split('=')
		
		# Data[2.5.4.17] is the zipcode, turns out it doesnt have an rc string		
		subject_data[data[0]] = data[1]
 
	
	cn = subject_data["CN"]

	# Removes the www option
	if cn.find('www') >= 0:
		cn = cn.replace('www.', '')
	elif cn.find('*.') >= 0:
		cn = cn.replace('*.', '')


	# Does the test to see if the host name matches
	if dns_summary.find(cn) >= 0: 
		print('Passed valid_sub_dns check')
		return 1
	


	return 0

# Checks revocation status of a cert
def revoke_check(args):	
	#i = os.system('sudo ./revoke_check.sh cert.pem')
	#i = os.system('wget example.com')
	#i = subprocess.run(['curl', 'example.com'])
	#sleep(2)
	print('CHECKING REVOKE')
	i = os.system('sudo ./revoke_check.sh cert.pem')
	print(i)
	return 1


def block_logjam(args):
	
	print('BLOCKING LOGJAM / CHECKING CIPHER SUITES')
	server_hello = args[3]
	len_following = server_hello[9:14]
	len_following = int(len_following.replace(' ', ''), 16)

	len_following_chars = 3 * len_following
	
	cipher_id = server_hello[44*3: ((44*3) + 5)]
	cipher_id = cipher_id.upper()

	named_cipher = cipher_dict[cipher_id.replace(' ', '')]

	try:
		with open('ciphers.conf', 'r') as fin:
			for line in fin.readlines():
				line = line.replace('\n', '')
				if line == named_cipher:
					print('Passed LJ')
					return 1
		return 0
	except:
		print("Cipher list not found. Check ciphers.conf")
		return 0
	
# Print cert data
def dump_cert(args):

	print("#####DUMPING#####")
	print("Issuer:        {0}".format(args[0].issuer))
	print("Subject:       {0}".format(args[0].subject))
	print("Serial Number: {0}".format(args[0].serial_number))
	print("Version:       {0}".format(args[0].version))
	print("Fingerprint:   {0}".format(args[0].fingerprint(hashes.SHA256())))


# Generates a string of leng num_words of just 00, used to wipe keys
def blank_key(num_words: int) -> str:
	ret = ''

	for i in range(0, num_words):
		ret += '00 '

	return ret[:-1]

