from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from yaml.loader import SafeLoader

import os
import hexdump
import re
import random
import time
import yaml
import threading


# Sends a reset packet
def send_reset(ip):
	test_reset = IP(src=ip[IP].dst, dst=ip[IP].src) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=ip[TCP].ack)		
	send(test_reset, iface='wlan0')
	test_reset = IP(src=ip[IP].src, dst=ip[IP].dst) / TCP(sport=ip[TCP].sport, dport=ip[TCP].dport, flags="R", seq=ip[TCP].seq)		
	send(test_reset, iface='wlan0')


def callback(pkt):
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	packet_in = IP(payload) # Creates a scapy packet at the IP level from the captured payload

	if packet_in.haslayer(TCP):
		#print(ip[TCP].flags, ip[TCP].seq, ip[TCP].ack)
		if 'A' in packet_in[TCP].flags:
			
			
			gl = IP(src=packet_in[IP].src, dst=packet_in[IP].dst) / \
			TCP(
			options=packet_in[TCP].options, \
			window=packet_in[TCP].window, \
			chksum=packet_in[TCP].chksum, \
			sport=packet_in[TCP].sport, \
			dport=packet_in[TCP].dport, \
			flags=packet_in[TCP].flags, \
			seq=packet_in[TCP].seq, \
			ack=packet_in[TCP].ack) / \
			Raw(
			load=packet_in[TCP].payload)
			
			
			send(gl, iface='wlan0')
			pkt.drop()
			return

	pkt.accept()


def main():
	net_filter = nfq()

	os.system('sudo iptables -I OUTPUT -j NFQUEUE --queue-num 5 --queue-bypass')	
	os.system('sudo iptables -I INPUT -j NFQUEUE --queue-num 6 --queue-bypass')
	
	net_filter.bind(5, callback)
	net_filter.bind(6, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()	

	return


if __name__ == "__main__":
	main()
