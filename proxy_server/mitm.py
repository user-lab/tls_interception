import socket
import ssl
import select
import os
import sys
import hashlib

from multiprocessing import Process
from connection_handle import handler
from time import sleep



table = {}
active_threads = []

def main():
    hostname = '127.0.0.1'
    port = 7095

    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.set_ciphers('TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:DHE-RSA-AES256-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:RSA-PSK-AES256-GCM-SHA384:DHE-PSK-AES256-GCM-SHA384:RSA-PSK-CHACHA20-POLY1305:DHE-PSK-CHACHA20-POLY1305:ECDHE-PSK-CHACHA20-POLY1305:AES256-GCM-SHA384:PSK-AES256-GCM-SHA384:PSK-CHACHA20-POLY1305:RSA-PSK-AES128-GCM-SHA256:DHE-PSK-AES128-GCM-SHA256:AES128-GCM-SHA256:PSK-AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:ECDHE-PSK-AES256-CBC-SHA384:ECDHE-PSK-AES256-CBC-SHA:SRP-RSA-AES-256-CBC-SHA:SRP-AES-256-CBC-SHA:RSA-PSK-AES256-CBC-SHA384:DHE-PSK-AES256-CBC-SHA384:RSA-PSK-AES256-CBC-SHA:DHE-PSK-AES256-CBC-SHA:AES256-SHA:PSK-AES256-CBC-SHA384:PSK-AES256-CBC-SHA:ECDHE-PSK-AES128-CBC-SHA256:ECDHE-PSK-AES128-CBC-SHA:SRP-RSA-AES-128-CBC-SHA:SRP-AES-128-CBC-SHA:RSA-PSK-AES128-CBC-SHA256:DHE-PSK-AES128-CBC-SHA256:RSA-PSK-AES128-CBC-SHA:DHE-PSK-AES128-CBC-SHA:AES128-SHA:PSK-AES128-CBC-SHA256:PSK-AES128-CBC-SHA')
    context.load_cert_chain(certfile="loop.pem", keyfile="loop.pem")

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0) as sock:
        sock.bind((hostname,port))
        sock.listen(5)
        #with context.wrap_socket(sock, server_side=True) as ssock:
        try:
            while True:
                conn, addr = sock.accept()
        
                key = addr[0] + str(addr[1])
                key = hashlib.sha256(key.encode()).hexdigest()
                val = (addr[0], addr[1])
                table[key] = val
                # <connects to>_con r, w
                parent_conn, child_conn = os.pipe()
                active_threads.append(parent_conn)

                p = Process(target = handler, args=(conn, addr, child_conn, parent_conn))
                p.start()
                
                rl, wl, xl = select.select(active_threads, [], [])
                for t in rl:
                    try:
                        fin = os.read(t, 1024)
                        fin = fin.decode()
                
                        table_del_key = fin[0:64]
                        del table[table_del_key]

                        thread_key = fin[64:]
                        active_threads.remove(thread_key)

                    except:
                        pass
                #tmp = os.read(parent_conn, 1024)
                #print(tmp)
        except KeyboardInterrupt:
            print('\nKilled')
            sys.exit(1)
                








if __name__ == "__main__":
    main()
