import os
import hashlib
import ssl
import socket
import select
import sys
from time import sleep


ssl.HAS_SNI = True
context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.set_ciphers('TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:DHE-RSA-AES256-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES128-SHA:DHE-RSA-AES128-SHA:RSA-PSK-AES256-GCM-SHA384:DHE-PSK-AES256-GCM-SHA384:RSA-PSK-CHACHA20-POLY1305:DHE-PSK-CHACHA20-POLY1305:ECDHE-PSK-CHACHA20-POLY1305:AES256-GCM-SHA384:PSK-AES256-GCM-SHA384:PSK-CHACHA20-POLY1305:RSA-PSK-AES128-GCM-SHA256:DHE-PSK-AES128-GCM-SHA256:AES128-GCM-SHA256:PSK-AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:ECDHE-PSK-AES256-CBC-SHA384:ECDHE-PSK-AES256-CBC-SHA:SRP-RSA-AES-256-CBC-SHA:SRP-AES-256-CBC-SHA:RSA-PSK-AES256-CBC-SHA384:DHE-PSK-AES256-CBC-SHA384:RSA-PSK-AES256-CBC-SHA:DHE-PSK-AES256-CBC-SHA:AES256-SHA:PSK-AES256-CBC-SHA384:PSK-AES256-CBC-SHA:ECDHE-PSK-AES128-CBC-SHA256:ECDHE-PSK-AES128-CBC-SHA:SRP-RSA-AES-128-CBC-SHA:SRP-AES-128-CBC-SHA:RSA-PSK-AES128-CBC-SHA256:DHE-PSK-AES128-CBC-SHA256:RSA-PSK-AES128-CBC-SHA:DHE-PSK-AES128-CBC-SHA:AES128-SHA:PSK-AES128-CBC-SHA256:PSK-AES128-CBC-SHA')
context.check_hostname = False
context.verify_mode = ssl.CERT_NONE

def handler(conn, addr, child_conn, parent_conn):
    server_ip = '127.0.0.1'
    server_port = '8081'
    
    # Generate the key to send back so that when we are done we can remove the thread and connection
    key = addr[0] + str(addr[1])
    print(key)
    conn_key = hashlib.sha256(key.encode()).hexdigest()
    conn_key+=str(parent_conn)
    print('connection')

    # Reads the data in from the connection
    #tmp = conn.recv(1024)

    with socket.create_connection((server_ip, server_port)) as sock:
        with context.wrap_socket(sock, server_hostname=server_ip) as ssock:
            #test = IP(src='127.0.0.1', dst='127.0.0.1')/Raw(b'Hello')	
        
            ssock.send(b'from_mitm')
            conn.send(b'from_mitm')
            active_descriptors = [ssock, conn]
            while True:
                #data = ssock.recv(1024)
            
                #print(data)
                #ssock.send(data)
                #conn.send(data)

                rl, wl, xl = select.select(active_descriptors, [], [])
                for t in rl:
                    try:
                        

                        fin = t.recv(1024)
                        print(fin)
                        
                        if t == conn:
                            ssock.send(fin)
                        else:
                            conn.send(fin)
                            
                        if fin == b'':
                            sys.exit(0)

                    except:
                        pass


    os.write(child_conn, conn_key.encode())
    #os.write(child_conn, str(parent_conn).encode())