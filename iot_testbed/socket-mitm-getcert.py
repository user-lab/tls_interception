#!/usr/bin/python3

from netfilterqueue import NetfilterQueue
from scapy.all import *
import hexdump
import socket


# iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1
# ./mitm-tls_downgrade.py
# echo | openssl s_client -connect ocw.cs.pub.ro:443
# iptables -D OUTPUT 1


# callback - called when a new packet is to be evaluated
#	@pkt - netfilterqueue Packet (accept / drop / ...)
def callback(pkt):
	
	load_layer('tls')
	# initialize a scapy ip packet with received data
	payload = pkt.get_payload()
	ip      = IP(payload)
	#ip.show()
	if not ip.haslayer("Raw"):                               # not the Handshake, forward
		pkt.accept()
		return
	else:
		tcpPayload = ip["Raw"].load

		print(tcpPayload)
	#print(bytes(payload))
	#print("#############################################")
	#hexdump.hexdump(payload)
	#hexdump.hexdump(bytes(ip['TCP'].payload))
	#print(bytes(ip['TCP'].payload))
	#print(bytes(ip['TLS'].payload))

	#pkt.accept()
	#return

	# no tcp content => not tls => let it pass	
	if not ip.haslayer('Raw'):
		pkt.accept()
		return

	# extract modifiable byte array representing tcp content (not header)
	tcp_payload = [it for it in ip['Raw'].load]

	print("#############################################")
	print(' '.join('{:02x}'.format(x) for x in tcp_payload))
	pkt.accept()
	return

	# tcp content is not server certificate => let it passs = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_TCP)

	if tcp_payload[:3] != [0x16, 0x03, 0x03]:
	   pkt.accept()
	   return

	if tcp_payload[5] == [0x0b]:
		print(tcp_payload)
		#print((hex(ar) for ar in tcp_payload))
		print(' '.join('{:02x}'.format(x) for x in tcp_payload))
		#print(bytes(tcp_payload).hex())
		pkt.accept()
		return

	# extract ciphersuite len (2 bytes per cipher)
	#cs_len = (tcp_payload[76] << 8) + tcp_payload[77]
	
	# replace all ciphersuites with only ECDHE-RSA-AES256-SHA (c0 13)
	#new_cs_len = int(cs_len / 2)
	#cyphers = [0xc0, 0x13] * new_cs_len
	# replace all ciphersuites with only ECDHE_RSA_WITH_AES_128_GCM_SHA256 (c0 2f)
	#cyphers = [0xc0, 0x2f] * new_cs_len
	
	#print(new_cs_len, " xxx ", len(cyphers))
	#tcp_payload = tcp_payload[:78]          + \
	#				cyphers  + \
	#				tcp_payload[78+cs_len:]

	# set new tcp content
	#ip['Raw'].load = bytes(tcp_payload)

	# modify length in ip header
	# delete checksums to force scapy to recalculate them
	del ip['IP'].chksum
	del ip['TCP'].chksum

	# overwrite netfilter packet with modified scapy packet and accept
	pkt.set_payload(ip.__bytes__())
	pkt.accept()


# main - program entry point
def main():
	# set up netfilter queue
	nfq = NetfilterQueue()
	nfq.bind(1, callback)
	nfq.bind(2, callback)

	


	try:
		nfq.run()
	except KeyboardInterrupt:
		pass
	finally:
		nfq.unbind()

if __name__ == '__main__':
	main()