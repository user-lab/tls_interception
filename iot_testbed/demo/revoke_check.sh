#!/usr/bin/bash

#:'
#	Author: John Sadik
#	Date Created: 17 September 2021
#	Date Last Modified: 21 September 2021
#	Description: 
#	This shell script will take a certificate file name on the command line 
#	and then verify the revocation status of that certificate. It does this
#	by first looking for a CA authority in the certificate and if it does not
#	find one, it will grab the certificate chain to get the CA authority. It
#	then uses the CA authority to check revocation stuats. The script returns
#	0 for a good certificate and 1 for a revoked certificate. 
#'

# Check command line arguments
if [ "$#" -ne 1 ]; then
	echo "Usage: sh revoke_check.sh name_of_cert.pem"
	exit
fi

# Assign command line arguments 
CERT=$1

# Getting OCSP URI
URI=$(sudo openssl x509 -in $CERT -noout -ocsp_uri)



# First, check the leaf node for a CA issuer
FOUND=0
LOCATION=$(sudo openssl x509 -in $CERT -text -noout | grep "CA Issuers")
if [ ! -z "$LOCATION" ]; then
	LOCATION=$(sudo echo $LOCATION | awk -F'URI:' '{print $2}')
	$(sudo curl -s $LOCATION --output individual-00)
	$(sudo openssl x509 -inform DER -in individual-00 -out individual-00)
	CURR=individual-00
	FOUND=1
fi

# If the leaf node doesn't have a CA issuer, then find the certificate chain and check there
if [ "$FOUND" -eq 0 ]; then 
	# Get Common Name
	URL=$(sudo openssl x509 -noout -subject -in $CERT | awk -F= '{print $NF}' | sed -e 's/^[ \t]*//')

	# Get certificate chain
	$(sudo openssl s_client -showcerts -connect $URL:443 < /dev/null 2>&1 | sed -n '/-----BEGIN/,/-----END/p' > collection.pem)

	# Split up certificates into separate parts
	$(sudo csplit -s -z -f individual- collection.pem '/-----BEGIN CERTIFICATE-----/' '{*}')

	# Count the possible choices 
	COUNT=$(sudo ls -dq *individual-0* | wc -l)
	
	# Iterate through the possible choices and find the correct one
	i=1
	while [ "$i" -lt $COUNT ]; do
		CURR=individual-0$i
		TEST=$(sudo openssl x509 -in $CURR -text -noout | grep "CA Issuers")
		if [ ! -z "$TEST" ]; then 
			FOUND=1 
			break
		fi
		i=$(( i + 1 ))
	done 
fi

# Run the verify command to get the data we need
$(sudo openssl ocsp -issuer $CURR -cert $CERT -text -url $URI 2>&1 | grep "Response verify" > my_out)
$(sudo openssl ocsp -noverify -issuer $CURR -cert $CERT -text -url $URI | grep "Cert Status" >> my_out)
$(sudo openssl ocsp -noverify -issuer $CURR -cert $CERT -text -url $URI | grep "$CERT" >> my_out)

# Check to make sure we got the right output
GOOD=$(sudo grep -c "good" my_out)
OK=$(sudo grep -c "Response verify OK" my_out)

if [ "$GOOD" -eq 2 -a "$OK" -eq 1 ]; then
	CODE=1
else CODE=0
fi

# Clean up
#rm individual-0*
#rm collection.pem 2> /dev/null
#rm my_out

# Exit with the proper code
exit $CODE
