class connection_data:

	def __init__ (self, src, dst, packet, sport = None, dport = None, ):
		self.src = src
		self.dst = dst
		self.sport = sport
		self.dport = dport
		self.packet = packet


class ZTN:


	# Whitelist
	traffic_allowed = {
		# Format
		#'0.0.0.0': [<con_data>, <con_data>]
	}

	
	# Green light communication to the client
	def add_to_traffic_whitelist(self, args):

		if self.traffic_allowed.get(args[0]) == None:
			self.traffic_allowed[args[0]] = {}

		self.traffic_allowed.get(args[0])[args[1].dst] = args[1]

	# Dumps output from the traffic whitelist	
	def dump_traffic_whitelist(self):
		for _, val in self.traffic_allowed.items():
			for _, con in val.items(): 
				print('{0} -> {1}'.format(con.src, con.dst))

	# Sees if we are allowed to get traffic
	def recieving_traffic(self, args) -> bool:
		cons = self.traffic_allowed.get(args[0]).get(args[1])
		if cons == None: return False

		return True
