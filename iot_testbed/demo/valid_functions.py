from valid_helpers import *

valid_functions = {
	
	'date': valid_date,
	'subject': valid_subject,
	'subject_dns': valid_subject_dns,
	'revoke': revoke_check,
	'self_signed': is_self_signed
			
	}


def add_to_captured(cert, captured_certs):
	captured_certs[cert.fingerprint(hashes.SHA256())] = cert

def was_captured(cert, captured_certs) -> int:
	if captured_certs.get(cert.fingerprint(hashes.SHA256())): return 1
	return 0

def add_to_banned(cert, captured_certs):
	captured_certs[cert.fingerprint(hashes.SHA256())] = cert

def is_banned(cert, captured_certs) -> int:
	if captured_certs.get(cert.fingerprint(hashes.SHA256())): return 1
	return 0
