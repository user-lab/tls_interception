'''
	Developed by Knox Cavitt, Bhaskar Gaur, and John Sadik
	
	Captures certificates, validates, makes verdict

'''


from netfilterqueue import NetfilterQueue as nfq # for collecting packets
from scapy.all import * # for parsing, creating, and working with packets
from valid_functions import * # functions used for validation
from ztn import ZTN, connection_data # beginning of ZTN implmenentation


import os
import hexdump
import re
import random
import time
import yaml
import threading

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from yaml.loader import SafeLoader


# global vars to maintain state
counter = 0
certificate_data = ['', 0]
client_hello = ''
curr_dns = ''
captured_certs = {}
banned_certs = {}

# flags
check_revoke_file = 0


# sends a reset packet
def send_reset(ip):
	test_reset = IP(src=ip[IP].dst, dst=ip[IP].src) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=ip[TCP].ack)		
	send(test_reset, iface='wlan0')


# called everytime we recieve a packet from nfq 
def callback(pkt):
	universal_args = [None, None, None]
	payload = pkt.get_payload() # gets the payload from the nfq packet
	ip = IP(payload) # creates a scapy packet at the IP level from the captured payload
		
	local_ztn  = ZTN()
	
	# checking to see if there are ports we can store
	if ip.haslayer(TCP):
		local_ztn.add_to_traffic_whitelist((ip[IP].src, connection_data(ip[IP].src, ip[IP].dst, ip, sport = ip[TCP].sport, dport = ip[TCP].dport)))
	else:
		local_ztn.add_to_traffic_whitelist((ip[IP].src, connection_data(ip[IP].src, ip[IP].dst, ip)))


	# checks to make sure we can recieve traffic from them
	if local_ztn.recieving_traffic((ip[IP].src, ip[IP].dst)) != True:
		pkt.drop()
		send_reset(ip)
		return

	# does the querry for the dns packets and sets curr dns, used for SNI checking
	if ip.haslayer(DNS):
		global curr_dns	
	
		summary = ip["DNS"].mysummary()

		if summary.find("DNS Qry") >= 0:
			curr_dns = summary


	# checking to see if the packet contains tcp data, used to capture certs
	if ip.haslayer("Raw"):
		global counter
		global certificate_data
		global client_hello
		
		counter += 1
		raw_load = ip["Raw"].load # grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		

		
		print("{0}: {1}".format(counter, kind_header))	
		print("{0} -> {1}".format(ip[IP].src, ip[IP].dst))
			
		# grabbing the client hello for subject name checking
		search = re.search('16 03 01', kind_load)
		if search != None:
			client_hello = kind_load

		# grabing the certificate
		search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load)

		# regex returns a none type if the pattern isnt found
		if search != None:

			# grabing the range of the match
			range = search.span()
			print("Found a match at range: {0}".format(search.span()))
			print("The certificate data: {0}".format(kind_load[range[0]:range[1]]))	

			# printing the size of the key and calculate the length as chars
			cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]	
			print("Cert key size: hex-{0} decimal-{1}".format(cert_size_hex_string, int(cert_size_hex_string.replace(' ', ''), 16)))
			cert_size_chars = int(cert_size_hex_string.replace(' ', ''), 16) * 3	
			

			# inititlizing the certificate data			
			certificate_data[1] = cert_size_chars - len(kind_load[range[0]+15:]) 
			certificate_data[0] += kind_load[range[0]+15:]
		
		elif int(certificate_data[1]) > int(0):

			# if I have enough room in my remaining chars to add the whole of the packet then I do
			if len(kind_load) <= int(certificate_data[1]):
				certificate_data[0] += kind_load
				certificate_data[1] -= len(kind_load)
			else:
				certificate_data[0] += kind_load[:int(certificate_data[1])]
				certificate_data[1] = 0

				# stores the hex string in a file
				crt = certificate_data[0][30:-2]
				fout = open('cert.txt', 'w')
				fout.write(crt)
				fout.close()
				

				# converts hex string to a DER and a PEM
				os.system('xxd -r -ps cert.txt cert.der; touch cert.pem ;openssl x509 -in cert.der -inform der -out cert.pem')
					
				# loads in the PEM using the cryptography library
				fin = open('cert.pem', 'rb')
				loaded_cert = x509.load_pem_x509_certificate(fin.read(), default_backend())
				fin.close()
				
				universal_args[0] = loaded_cert
				universal_args[1] = client_hello
				universal_args[2] = curr_dns

				certificate_data = ['', 0]		
		
				####Certificate Validations###
				
				# if the certificate is in the banned list drop it
				if is_banned(loaded_cert, banned_certs) == 1:
					print("Banned Cert!!!")	
					send_reset(ip)
					pkt.drop()
					return
				

				# if the cert was not previously captured, validate it
				if was_captured(loaded_cert, captured_certs) != 1: 
					

					results =  0#CHANGE ME TO -1 TO MAKE CERTS INVALID
					config_options = None

					# load validation options
					with open('config.yaml') as conf:
						config_options = yaml.load(conf, Loader=SafeLoader)
					
					# validate 
					for entry in config_options['ValidationOptions']:	
						# need to implement threading / mp here
						#if entry == 'revoke':
							#r_thread = threading.Thread(target=valid_functions[entry])
							#r_thread.start()
						#	results += 1
							#results += valid_functions[entry](universal_args)
						#else:
						results += valid_functions[entry](universal_args)

						print('Calling: {0}  Results: {1} '.format(entry, results))

					#Bhaskar - commented blacklisting and whitelisting for testing
					# make sure every check returned valid
					if len(config_options['ValidationOptions']) != int(results):
						print("INVALID!")
						#Blacklisting of Certs
						#add_to_banned(loaded_cert, banned_certs)	

						if ip.haslayer(TCP):
							print('I have a layer and I\'m bad')
							send_reset(ip)		
							pkt.drop()
							return
					else:
						print('VALID!!')
						#Whitelisted Certs
						#add_to_captured(loaded_cert, captured_certs)
						
					
					dump_cert(universal_args)
				else:
					print("Already Validated!!!")


	pkt.accept()


def main():
	net_filter = nfq()

	#os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --sport 443 -j NFQUEUE --queue-num 3 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --dport 443 -j NFQUEUE --queue-num 4 --queue-bypass')
	os.system('sudo iptables -I OUTPUT -j NFQUEUE  --queue-num 5 --queue-bypass')	
	os.system('sudo iptables -I INPUT -j NFQUEUE   --queue-num 6 --queue-bypass')
	#os.system('sudo iptables -I OUTPUT -j NFQUEUE -s 127.0.0.1 -d 127.0.0.1 --queue-num 5 --queue-bypass')	
	#os.system('sudo iptables -I INPUT -j NFQUEUE -s 127.0.0.1 -d 127.0.0.1  --queue-num 6 --queue-bypass')
	
	#net_filter.bind(1, callback)
	#net_filter.bind(2, callback)	
	#net_filter.bind(3, callback)
	#net_filter.bind(4, callback)
	net_filter.bind(5, callback)
	net_filter.bind(6, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()
		os.system('sudo iptables -F')

	return


if __name__ == "__main__":
	main()
