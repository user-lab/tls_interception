'''
	Functions used to validate a x509 certificate via cryptography library
'''


from cryptography.hazmat.primitives import hashes
from datetime import datetime
from cryptography.x509.oid import NameOID
from cryptography.x509 import NameAttribute
from time import sleep

import os

# Checks if self signed
def is_self_signed(args) -> int:
	cert = None
	cert = args[0]
	print(cert.issuer)
	print(cert.subject)
	if cert.issuer == cert.subject:
		print("Detected self signed certificate:")
		return 0

	return 1

# Checks the valid date
def valid_date(args) -> int:
	cert = None
	cert = args[0]

	if cert.not_valid_before > datetime.now(): return 0
	if cert.not_valid_after < datetime.now(): return 0

	return 1

# Checks the CN from the client hello with the CN of the certificate
def valid_subject(args) -> int:
	cert = args[0] 
	client_hello = args[1]
	
	
	subject = cert.subject 
	

	# Gets all of the data 
	subject_data = {}
	
	for entry in subject:
		data = entry.rfc4514_string()
		data = data.split('=')
		
		# Data[2.5.4.17] is the zipcode, turns out it doesnt have an rc string		
		subject_data[data[0]] = data[1]
	

	# Claculates all of the offset to client_hello ends up being just the extensions
	client_hello = client_hello[3 * 43:]
	len_of_sessionid = client_hello[0:2]
	i_len_of_sessionid = int(len_of_sessionid.replace(' ', ''), 16)
	client_hello = client_hello[(i_len_of_sessionid+1) * 3:]
	i_len_of_suite = int(client_hello[0:5].replace(' ', ''), 16)
	client_hello = client_hello[(i_len_of_suite + 2) * 3:]

	# Piece togther the hex string 
	to_test = ''.join(chr(int(i, 16)) for i in client_hello.split())

	cn = subject_data["CN"]

	# Removes the www option
	if cn.find('www') >= 0:
		cn = cn.replace('www.', '')

	# Does the test to see if the host name matches
	if to_test.find(cn) >= 0: return 1
	
	return 0


def valid_subject_dns(args) -> int:
	cert = args[0]
	dns_summary = args[2]

	subject = cert.subject 
	

	# Gets all of the data 
	subject_data = {}
	
	for entry in subject:
		data = entry.rfc4514_string()
		data = data.split('=')
		
		# Data[2.5.4.17] is the zipcode, turns out it doesnt have an rc string		
		subject_data[data[0]] = data[1]
 
	
	cn = subject_data["CN"]

	# Removes the www option
	if cn.find('www') >= 0:
		cn = cn.replace('www.', '')


	# Does the test to see if the host name matches
	if dns_summary.find(cn) >= 0: return 1
	


	return 0

# Checks revocation status of a cert
def revoke_check(args):	
	#i = os.system('sudo ./revoke_check.sh cert.pem')
	#i = os.system('wget example.com')
	#i = subprocess.run(['curl', 'example.com'])
	#sleep(2)
	print('im doing')
	i = os.system('sudo sh revoke_check.sh cert.pem')
	return i


# Print cert data
def dump_cert(args):

	print("#####DUMPING#####")
	print("Issuer:        {0}".format(args[0].issuer))
	print("Subject:       {0}".format(args[0].subject))
	print("Serial Number: {0}".format(args[0].serial_number))
	print("Version:       {0}".format(args[0].version))
	print("Fingerprint:   {0}".format(args[0].fingerprint(hashes.SHA256())))


# Generates a string of leng num_words of just 00, used to wipe keys
def blank_key(num_words: int) -> str:
	ret = ''

	for i in range(0, num_words):
		ret += '00 '

	return ret[:-1]

