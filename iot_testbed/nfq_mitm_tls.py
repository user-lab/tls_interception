#!/usr/bin/python3

from netfilterqueue import NetfilterQueue
from scapy.all import *
import os
from scapy.layers.tls.record import TLS


def print_and_accept(pkt):
    print(pkt,"   ",pkt.get_payload(),"   ",pkt.get_payload_len(),"   ",pkt.get_hw())
    payload = pkt.get_payload()
    
    pkt1 = IP(payload)
    print("Got a packet ! source ip : %s dest: %s" % (str(pkt1.src), str(pkt1.dst)))
    #print("Content is: ",pkt1[TCP].payload)
    
    pkt1[TCP].payload = bytes(pkt1[TCP].payload).replace(b'Message',b'MessagE')
    
    #pkt1.show()
    
    pkt.accept()

def callback(pkt):
    load_layer('tls')

    # initialize a scapy ip packet with received data
    payload = pkt.get_payload()
    ip      = IP(payload)
    print(str(ip[TCP].flags))
    print(ip[TCP])

    if not ip.haslayer('Raw'):
        pkt.accept()
        return

    tcp_payload = [it for it in ip['Raw'].load]

    if tcp_payload[:3] != [0x16, 0x03, 0x01]:
        pkt.accept()
        return

    if ip.haslayer(TLS):
        print("got TLS##############")
        print(ip[TLS].show())
        pkt.accept()
        return

    if ip.haslayer(TCP) and ip[TCP].payload is not '':
        #print("Got a packet ! source ip : %s dest: %s" % (str(ip.src), str(ip.dst)))
        print("Got a packet ! source ip : %s dest: %s" % ((ip.src), (ip.dst)))
        tcp_payload = bytes(ip[TCP].payload).replace(b'Message',b'Mes')
        print(tcp_payload)
        
        #payload_before = len(ip[TCP].payload)
        #payload_after = len(tcp_payload)
        #payload_dif = payload_after - payload_before
        #ip[IP].len = ip[IP].len + payload_dif

        #ip[IP].ttl = 40

        #print('Data: '+ str(ip[TCP].payload))
        #print('TTL: ' + str(ip[IP].ttl))

        # set new tcp content
        ip[Raw].load = tcp_payload

        del ip[IP].len
        del ip[IP].chksum
        del ip[TCP].chksum
        #pkt.set_verdict_modified(NetfilterQueue.NF_ACCEPT, str(ip), len(ip))
        #del ip[TCP].len
        
        pkt.set_payload(ip.__bytes__())
    
    
    # extract modifiable byte array representing tcp content (not header)
    #tcp_payload = [it for it in ip['Raw'].load]

    #print(tcp_payload)

    #del ip['TCP'].len
    #del ip['TCP'].chksum

    # overwrite netfilter packet with modified scapy packet and accept
    #pkt.set_payload(ip.__bytes__())
    pkt.accept()

# main - program entry point
def main():
    cmd = "sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass"
    #returned_value = os.system(cmd)  # returns the exit code in unix
    #print('Starting iptables nfqueue: ', bool(~returned_value))

    # set up netfilter queue
    nfq = NetfilterQueue()
    nfq.bind(1, callback)

    try:
        nfq.run()
    except KeyboardInterrupt:
        pass
    finally:
        nfq.unbind()
        cmd = "sudo iptables -D OUTPUT 1"
        #returned_value = os.system(cmd)  # returns the exit code in unix
        #print('Ending iptables nfqueue: ', bool(~returned_value))

if __name__ == '__main__':
	main()
    
