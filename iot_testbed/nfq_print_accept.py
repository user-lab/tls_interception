from netfilterqueue import NetfilterQueue
from scapy.all import *
import os

def print_and_accept(pkt):
    print(pkt,"   ",pkt.get_payload(),"   ",pkt.get_payload_len(),"   ",pkt.get_hw())
    payload = pkt.get_payload()
    
    pkt1 = IP(payload)
    print("Got a packet ! source ip : %s dest: %s" % (str(pkt1.src), str(pkt1.dst)))
    #print("Content is: ",pkt1[TCP].payload)
    
    pkt1[TCP].payload = bytes(pkt1[TCP].payload).replace(b'Message',b'MessagE')
    
    #pkt1.show()
    
    pkt.accept()

nfqueue = NetfilterQueue()

nfqueue.bind(1, print_and_accept)
try:
    nfqueue.run()
except KeyboardInterrupt:
    print('')

nfqueue.unbind()
