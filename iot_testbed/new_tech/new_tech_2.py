'''
	Developed by Knox Cavitt, Bhaskar Gaur, and John Sadik
	Captures and prints certificates
'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *


import os
import hexdump
import re
import random
import time
#import yaml
import threading
import multiprocessing as mp

from cryptography import x509
from cryptography.hazmat.backends import default_backend
#from yaml.loader import SafeLoader


# Global Vars to maintain state
counter = 0
certificate_data = ['', 0]
client_hello = ''
curr_dns = ''
source = ''
destination = ''
block_acks = 0
client_ip = ''
server_ip = ''
client_port = ''
server_port = ''

# Might need to change to hashmap as per Dr.Ruoti
captured_certs = {}
banned_certs = {}
connections = {}
packets_to_pass = []


# Sends a reset packet
def send_reset(ip):
	test_reset = IP(src=ip[IP].dst, dst=ip[IP].src) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=ip[TCP].ack)		
	send(test_reset, iface='wlan0')

# Sends acks to both client and server
def send_ack(ip):
	
	length = 1
		
	if len(ip[TCP].payload) != 0:
		length = len(ip[TCP].payload)
	
	server_ack = IP(src=ip[IP].dst, dst=ip[IP].src) / \
	TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="A", seq=ip[TCP].ack, ack=ip[TCP].seq + length)	
	#Raw(ip[TCP].payload)
	
	client_ack = IP(src=ip[IP].src, dst=ip[IP].dst) / \
	TCP(sport=ip[TCP].sport, dport=ip[TCP].dport, flags="A", seq=ip[TCP].ack, ack=ip[TCP].seq + length) 
	#Raw(ip[TCP].payload) 

	send(server_ack, iface='wlan0')
	send(client_ack, iface='wlan0')

# Ack looping while validating
def ack_loop(ip):
	ip = ip[0]
	send_ack(ip)
	print('ack loop')
	time.sleep(.5)

def get_tcp_seg_len(ip):
    ip_total_len = ip.getlayer(IP).len
    ip_header_len = ip.getlayer(IP).ihl * 32 / 8
    tcp_header_len = ip.getlayer(TCP).dataofs * 32 / 8
    tcp_seg_len = ip_total_len - ip_header_len - tcp_header_len
    return tcp_seg_len

def print_seq_ack(ip):
	print("SEQ " + str(ip.getlayer(TCP).seq) +" ACK " + str(ip.getlayer(TCP).ack) + " TCP seg len: " + str(get_tcp_seg_len(ip)))


'''
Intercepts ack from client to server.
Intercepts only zero length pure acks.
Sends same packet instead by Scapy and asks nfq to drop its ack.
'''
def send_ack_from_client_to_server(ip):

	try:
		if 'A' in ip[TCP].flags and get_tcp_seg_len(ip)==0 and len(str(ip[TCP].flags))==1 \
			and client_ip==(str(ip[IP].src)).strip() and server_ip==(str(ip[IP].dst)).strip() \
			and client_port==(str(ip[IP].sport)).strip() and server_port==(str(ip[IP].dport)).strip():
			print_seq_ack(ip)
			print(ip.mysummary(), str(ip[TCP].flags))
			
			try:
				print("-----------------------------------------------")
				send(ip, iface = 'wlan0')
				return 1
			except:
				print('#')
				return 0
			
	except:	
		print('@')
		return 0

'''
Intercepts ack from client to server.
Intercepts only non-zero length acks which accompany other packets.
Strips those packets of their acks.
Sends those packets instead by Scapy and asks nfq to drop that packet.
Also sends the ack after that packet using Scapy.
'''
def strip_ack_from_client_to_server(ip):
	try:
		if 'A' in ip[TCP].flags and get_tcp_seg_len(ip)>0 and len(str(ip[TCP].flags))>1 \
			and client_ip==(str(ip[IP].src)).strip() and server_ip==(str(ip[IP].dst)).strip() \
			and client_port==(str(ip[IP].sport)).strip() and server_port==(str(ip[IP].dport)).strip():
			#remove the ack
			ip[TCP].flags = str(ip[TCP].flags).replace('A','')
			print(ip[TCP].flags)
			print_seq_ack(ip)
			print(ip.mysummary(), str(ip[TCP].flags))
			
			ack_2server = IP(src=ip[IP].src, dst=ip[IP].dst) / \
	TCP(sport=ip[IP].sport, dport=ip[IP].dport, flags="A", seq=ip[TCP].seq, ack=ip[TCP].ack)
			

			try:
				print("-----------------------------------------------")
				send(ip, iface = 'wlan0')
				send(ack_2server, iface = 'wlan0')

				return 1
			except:
				print('#')
				return 0
			
	except:	
		print('@')
		return 0

def callback(pkt):
	global counter
	global certificate_data
	global client_hello
	global source
	global destination
	global block_acks
	global packets_to_pass
	global client_ip
	global server_ip
	global client_port
	global server_port
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload

	#print(ip.mysummary())
	
	# Checking to see if the packet contains tcp data, used to capture certs
	if ip.haslayer("Raw"):
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]

		# Grabbing the client hello for subject name checking
		search = re.search('16 03 01', kind_load)
		if search != None:
			client_hello = kind_load
			client_ip = (str(ip[IP].src)).strip()
			client_port = (str(ip[TCP].sport)).strip()
			server_ip = (str(ip[IP].dst)).strip()
			server_port = (str(ip[TCP].dport)).strip()

			print("client: ", client_ip,":",client_port, " server: ",server_ip,":",server_port)
			

	# Do the block, needs to be tested
	if ip.haslayer(TCP):
		if (strip_ack_from_client_to_server(ip) == 1):
			pkt.drop()
			return

	pkt.accept()
	return


def main():
	net_filter = nfq()
	#Might need to remove tcp protocol as DNS packets need to be intercepted in future, sport/dport make sure that on tls data is caught	
	os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 4 --queue-bypass')
	os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 4 --queue-bypass')

	net_filter.bind(4, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()	
		
		os.system('sudo iptables -F')	

	return


if __name__ == "__main__":
	main()
