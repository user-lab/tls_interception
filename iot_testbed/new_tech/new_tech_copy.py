'''
	Developed by Knox Cavitt, Bhaskar Gaur, and John Sadik
	Captures and prints certificates
'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *


import os
import hexdump
import re
import random
import time
import yaml
import threading

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from yaml.loader import SafeLoader


# Global Vars to maintain state
counter = 0
certificate_data = ['', 0]
client_hello = ''
curr_dns = ''
source = ''
destination = ''
block_acks = 0


# Might need to change to hashmap as per Dr.Ruoti
captured_certs = {}
banned_certs = {}
connections = {}


# Sends a reset packet
def send_reset(ip):
	test_reset = IP(src=ip[IP].dst, dst=ip[IP].src) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=ip[TCP].ack)		
	send(test_reset, iface='wlan0')


def send_ack(ip):
	#print(len(ip[TCP].payload)i
	
	length = 1
	
	if len(ip[TCP].payload) != 0:
		length = len(ip[TCP].payload)
	
	server_ack = IP(src=ip[IP].dst, dst=ip[IP].src) / \
	TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="A", seq=ip[TCP].ack, ack=ip[TCP].seq + length)	/ \
	Raw(ip[TCP].payload)
	
	client_ack = IP(src=ip[IP].src, dst=ip[IP].dst) / TCP(sport=ip[TCP].sport, dport=ip[TCP].dport, flags="A", seq=ip[TCP].ack, ack=ip[TCP].seq + length) /Raw(ip[TCP].payload) 

	send(server_ack, iface='wlan0')
	send(client_ack, iface='wlan0')


def callback(pkt):
	global counter
	global certificate_data
	global client_hello
	global source
	global destination
	global block_acks
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload

	accept = 1	

	print(ip.mysummary())

	# Do the block
	if block_acks == 1 and ip.haslayer(TCP):
		try:
			if 'A' in ip[TCP].flags and connections[ip.src] == ip.dst:
				pkt.drop()
				return 
		except:
			pass


	# Checking to see if the packet contains tcp data, used to capture certs
	if ip.haslayer("Raw"):


		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		
	

		# Grabbing the client hello for subject name checking
		search = re.search('16 03 01', kind_load)
		if search != None:
			client_hello = kind_load

		# Grabing the certificate
		search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load)

		# Regex returns a none type if the pattern isnt found
		if search != None:
			
			if source == '' and destination == '':
				source = ip.src
				destination = ip.dst
				connections[source] = destination
				connections[destination] = source

				block_acks = 1

			# Grabing the range of the match
			range = search.span()
			#print("Found a match at range: {0}".format(search.span()))
			#print("The certificate data: {0}".format(kind_load[range[0]:range[1]]))	

			# Printing the size of the key and calculate the length as chars
			cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]	
			#print("Cert key size: hex-{0} decimal-{1}".format(cert_size_hex_string, int(cert_size_hex_string.replace(' ', ''), 16)))
			cert_size_chars = int(cert_size_hex_string.replace(' ', ''), 16) * 3	
			

			# Inititlizing the certificate data			
			certificate_data[1] = cert_size_chars - len(kind_load[range[0]+15:]) 
			certificate_data[0] += kind_load[range[0]+15:]
			#print('Sending an ack...')
			#print('Dropping packet')
			send_ack(ip)	
			accept = 0	
		
		elif int(certificate_data[1]) > int(0):

			# If I have enough room in my remaining chars to add the whole of the packet then I do
			if len(kind_load) <= int(certificate_data[1]):
				certificate_data[0] += kind_load
				certificate_data[1] -=len(kind_load)
				#print('Sending an ack...')
				#print('Dropping packet')
				#send_ack(ip)
				accept=1
			else:
				certificate_data[0] += kind_load[:int(certificate_data[1])]
				certificate_data[1] = 0

				# Stores the hex string in a file
				crt = certificate_data[0][30:-2]
				fout = open('cert.txt', 'w')
				fout.write(crt)
				fout.close()
					
				'''	
				# Send the certificate packet

				alt_load_bytes = bytes.fromhex(crt)
				alt_pack = IP(alt_load_bytes)
				
				pkt.set_payload(alt_pack.__bytes__())	
			
				accept = 1
				
				# Converts hex string to a DER and a PEM
				os.system('xxd -r -ps cert.txt cert.der; touch cert.pem ;openssl x509 -in cert.der -inform der -out cert.pem')
					
				# Loads in the PEM using the cryptography library
				fin = open('cert.pem', 'rb')
				loaded_cert = x509.load_pem_x509_certificate(fin.read(), default_backend())
				fin.close()
	
				print('Loop sending an ack...')
				print('Valid / Invalid')
				'''
				source = ''
				destination = ''
				block_acks = 0


	if accept != 1:
		pkt.drop()
	else:
		pkt.accept()

	return


def main():
	net_filter = nfq()

	#os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --sport 443 -j NFQUEUE --queue-num 3 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --dport 443 -j NFQUEUE --queue-num 4 --queue-bypass')
	os.system('sudo iptables -I OUTPUT -j NFQUEUE --queue-num 1 --queue-bypass')	
	os.system('sudo iptables -I INPUT -j NFQUEUE --queue-num 1 --queue-bypass')	
	#os.system('sudo iptables -I INPUT -p tcp --tcp-flags ALL ALL -j NFQUEUE --queue-num 7 --queue-bypass')
	#net_filter.bind(1, callback)
	#net_filter.bind(2, callback)	
	#net_filter.bind(3, callback)
	#net_filter.bind(4, callback)
	#net_filter.bind(5, callback)
	net_filter.bind(1, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()	
		
		os.system('sudo iptables -F')	

	return


if __name__ == "__main__":
	main()
