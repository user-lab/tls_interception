'''
	Developed by Knox Cavitt and Bhaskar Gaur
	Capture tcp packets with loads that begin with 16 03 03 00

'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *

import os
import hexdump
import re
import time

counter = 0
block = 2

def blank_key(num_words: int) -> str:
	ret = ''

	for i in range(0, num_words):
		ret += '00 '

	return ret[:-1]

def change_server_common_name(kind_load: str, range: Tuple[int, int]):
	common_name_size_loc = 154
	x = range[1] + common_name_size_loc*3

	print("The certificate data: {0}".format(kind_load[range[0]:x]))
	
	common_name_offset = int(kind_load[x-2:x], 16) * 3
	common_name = kind_load[x:x+common_name_offset]
	#change common_name of the server in the certificate by replacing last two letters with 'aa'
	common_name = common_name[:-5] + "61 61"
	print("The common name: {0}".format(common_name))
	
	alt_load = kind_load[:x] + common_name + kind_load[x+common_name_offset+1:]
	#print(kind_load)
	print("###############################")
	alt_load.replace('\n','')
	print(alt_load[690:710])

	return alt_load

def change_server_public_key(kind_load: str, range: Tuple[int, int]):
	first_cert_start = 9 	#The first cert starts at 10th byte.
	cert_seq_length = 8  	#Next 8 bytes for cert sequence info, this is static.

	x = range[1] + (first_cert_start + cert_seq_length + 1)*3
	version_offset = int(kind_load[x:x+3], 16) * 3
	print("x: ", x, "version_offset: ", version_offset, "actual: ", kind_load[x:x+3])

	x = x + version_offset + 2*3
	serial_offset = int(kind_load[x:x+3], 16) * 3
	print("serial_offset: {0}".format(serial_offset), "actual: ", kind_load[x:x+3])

	x = x + serial_offset + 2*3
	algo_offset = int(kind_load[x:x+3], 16) * 3
	print("algo_offset: {0}".format(algo_offset), "actual: ", kind_load[x:x+3])

	x = x + algo_offset + 2*3
	issuer_offset = int(kind_load[x:x+3], 16) * 3
	print("issuer_offset: {0}".format(issuer_offset), "actual: ", kind_load[x:x+3])

	x = x + issuer_offset + 2*3
	validity_offset = int(kind_load[x:x+3], 16) * 3
	print("validity_offset: {0}".format(validity_offset), "actual: ", kind_load[x:x+3])

	x = x + validity_offset + 2*3
	subject_offset = int(kind_load[x:x+3], 16) * 3
	print("subject_offset: {0}".format(subject_offset), "actual: ", kind_load[x:x+3])

	x = x + subject_offset + 6*3
	pki_sequence_offset = int(kind_load[x:x+3], 16) * 3
	print("pki_sequence_offset: {0}".format(pki_sequence_offset), "actual: ", kind_load[x:x+3])

	x = x + pki_sequence_offset + 3*3
	public_key_offset = int(kind_load[x:x+6].replace(" ",""), 16) * 3
	print("public_key_offset: {0}".format(public_key_offset), "actual: ", kind_load[x:x+6])

	public_key_size = public_key_offset-1*3
	x = x + 3*3
	public_key = kind_load[x:x+public_key_size]
	print("Public key: {0}".format(public_key))

	out_key = blank_key(public_key_size) #public_key[:-2] + '00'
	#print(out_key)

	#alt_load = kind_load[:x] + out_key + kind_load[x+public_key_size+1:]
	alt_load = kind_load[:x] + public_key + kind_load[x+public_key_size+1:]
	time.sleep(3)
	alt_load.replace('\n','')
	
	#print("###############################")
	#print(alt_load)

	return alt_load

	common_name_size_loc = 154
	x = range[1] + common_name_size_loc*3

	print("The certificate data: {0}".format(kind_load[range[0]:x]))
	
	common_name_offset = int(kind_load[x-2:x], 16) * 3
	common_name = kind_load[x:x+common_name_offset]
	#change common_name of the server in the certificate by replacing last two letters with 'aa'
	common_name = common_name[:-5] + "61 61"
	print("The common name: {0}".format(common_name))
	
	alt_load = kind_load[:x] + common_name + kind_load[x+common_name_offset+1:]
	#print(kind_load)
	print("###############################")
	alt_load.replace('\n','')
	print(alt_load[690:710])

	return kind_load

def callback(pkt):
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload
	
	# Checking to see if the packet contains tcp data
	if ip.haslayer("Raw"):
		global counter
		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		

		
		print("{0}: {1}".format(counter, kind_header))	

		# Type version length handshake-type length curve-type named-curve pubkey-length
		search = re.search('16 03 03 .. .. 0b', kind_load)
		


		# Regex returns a none type if the pattern isnt found
		if search != None:
			global block
			block -= 1
			print(block)

			# Grabing the range of the match
			range = search.span()
			print("Found a match at range: {0}".format(search.span()))

			#alt_load = change_server_common_name(kind_load, range)
			alt_load = change_server_public_key(kind_load, range)

			#alt_load_array = alt_load.split(' ')
			alt_load_bytes = bytes.fromhex(alt_load)
			ip['Raw'].load = alt_load_bytes

			# modify length in ip header
			# delete checksums to force scapy to recalculate them
			del ip['IP'].chksum
			del ip['TCP'].chksum

			# overwrite netfilter packet with modified scapy packet and accept
			pkt.set_payload(ip.__bytes__())
			#pkt.accept()
			
			# Printing the size of the key and calculate the length as chars
			#pub_size_hex_string = kind_load[range[1]-2 : range[1]]	
			#print("Pub key size: hex-{0} decimal-{1}".format(pub_size_hex_string, int(pub_size_hex_string, 16)))
			#key_offset = int(pub_size_hex_string, 16) * 3

			#public_key = kind_load[ range[1] : range[1] + key_offset]

			# Print the public key
			#print("Pub key pre-op:{0}".format(public_key))
			
			
			
			# Constructing the altered packet
			#out_key = blank_key(int(pub_size_hex_string, 16)) #public_key[:-2] + '00'
			#print(out_key)	

			#if block <= 0:
			#alt_load = kind_load[:range[1]] + public_key + kind_load[range[1]+key_offset:]
			#else:	
			#alt_load = kind_load[:range[1]] + out_key + kind_load[range[1]+key_offset:]



			#alt_load_bytes = bytes.fromhex(alt_load)

			#alt_ip = IP(alt_load_bytes)
			#del alt_ip['IP'].chksum


			#hexdump.hexdump(raw_load)	
			
			#pkt.set_payload(alt_ip.__bytes__())
				

	pkt.accept()


def main():
	net_filter = nfq()

	#os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')

	net_filter.bind(1, callback)
	net_filter.bind(2, callback)
	

	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()

	

	return


if __name__ == "__main__":
	main()
