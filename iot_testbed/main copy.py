'''
	Developed by Knox Cavitt and Bhaskar Gaur
	Capture tcp packets with loads that begin with 16 03 03 00

'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *
import os
import hexdump

counter = 0

def callback(pkt):
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload
	
	# Checking to see if the packet contains tcp data
	if ip.haslayer("Raw"):
		global counter
		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_init = kind_load[:12] 
		#kind_init = kind_load[:2]		

		# Tests the packet to see if the first byte indicates a server hello - client key exchange is also in this family
		print("{0}: {1}".format(counter, kind_init))
		hexdump.hexdump(raw_load)

		#if ip.haslayer("TLS"):
		#	print("was TLS#########")

		#if kind_init.replace(' ', '') == "16":
			#print("{0}: {1}".format(counter, kind_init))
			#hexdump.hexdump(raw_load)
		#else:
			#hexdump.hexdump(raw_load)

	pkt.accept()


def main():
	net_filter = nfq()
	#sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass
	#os.system('sudo iptables -I OUTPUT -i wlan0 -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')

	net_filter.bind(1, callback)
	net_filter.bind(2, callback)
	

	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()

	

	return


if __name__ == "__main__":
	main()
