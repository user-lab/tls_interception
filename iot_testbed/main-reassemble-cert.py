'''
	Developed by Knox Cavitt and Bhaskar Gaur
	Capture tcp packets with loads that begin with 16 03 03 00

'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *

import os
import hexdump
import re

counter = 0
certificate_data = ['', 0]


def blank_key(num_words: int) -> str:
	ret = ''

	for i in range(0, num_words):
		ret += '00 '

	return ret[:-1]


def callback(pkt):
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload
	
	# Checking to see if the packet contains tcp data
	if ip.haslayer("Raw"):
		global counter
		global certificate_data
		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		

		
		print("{0}: {1}".format(counter, kind_header))	

		# Type version length handshake-type length curve-type named-curve pubkey-length
		search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load)
		


		# Regex returns a none type if the pattern isnt found
		if search != None:

			# Grabing the range of the match
			range = search.span()
			print("Found a match at range: {0}".format(search.span()))
			print("The certificate data: {0}".format(kind_load[range[0]:range[1]]))
			
			

			# Printing the size of the key and calculate the length as chars
			cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]	
			print("Cert key size: hex-{0} decimal-{1}".format(cert_size_hex_string, int(cert_size_hex_string.replace(' ', ''), 16)))
			cert_size_chars = int(cert_size_hex_string.replace(' ', ''), 16) * 3	
			
			
			certificate_data[1] = cert_size_chars - len(kind_load[range[0]+15:]) 
			certificate_data[0] += kind_load[range[0]+15:]
		
			print("Updated")
		elif int(certificate_data[1]) > int(0):


			# If I have enough room in my remaining chars to add the whole of the packet then I do
			if len(kind_load) <= int(certificate_data[1]):
				certificate_data[0] += kind_load 
				certificate_data[1] -= len(kind_load)
			else:
				certificate_data[0] += kind_load[:int(certificate_data[1])]
				certificate_data[1] = 0
		
				to_convert = certificate_data[0][30:-2]
				converted_64 = to_convert.encode('Base64')
				
		
	
	pkt.accept()


def main():
	net_filter = nfq()

	#os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --sport 443 -j NFQUEUE --queue-num 3 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --dport 443 -j NFQUEUE --queue-num 4 --queue-bypass')
	
	net_filter.bind(1, callback)
	net_filter.bind(2, callback)	
	#net_filter.bind(3, callback)
	#net_filter.bind(4, callback)

	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		global certificate_data
		net_filter.unbind()
		


	

	return


if __name__ == "__main__":
	main()
