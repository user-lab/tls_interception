#!/usr/bin/python3

from netfilterqueue import NetfilterQueue
from scapy.all import *
import os

prev_ip = ''

def print_and_accept(pkt):
    print(pkt,"   ",pkt.get_payload(),"   ",pkt.get_payload_len(),"   ",pkt.get_hw())
    payload = pkt.get_payload()
    
    pkt1 = IP(payload)
    print("Got a packet ! source ip : %s dest: %s" % (str(pkt1.src), str(pkt1.dst)))
    #print("Content is: ",pkt1[TCP].payload)
    
    pkt1[TCP].payload = bytes(pkt1[TCP].payload).replace(b'Message',b'MessagE')
    
    #pkt1.show()
    
    pkt.accept()

def next_seq_ack(ip):
    #flag, seq, ack, len
    nseq = ip.getlayer(TCP).seq
    nack = ip.getlayer(TCP).ack
    return (nseq, nack)

def get_tcp_seg_len(ip):
    ip_total_len = ip.getlayer(IP).len
    ip_header_len = ip.getlayer(IP).ihl * 32 / 8
    tcp_header_len = ip.getlayer(TCP).dataofs * 32 / 8
    tcp_seg_len = ip_total_len - ip_header_len - tcp_header_len
    return tcp_seg_len

def callback(pkt):
    # initialize a scapy ip packet with received data
    payload = pkt.get_payload()
    ip      = IP(payload)
    global prev_ip

    if prev_ip != '':
        (nseq, nack) = next_seq_ack(prev_ip)
        print("-------------------------------------------")
        #print("Next SEQ " + str(nseq) +" ACK " + str(nack))    
    prev_ip = ip

    #print(ip)
    #print(str(ip[TCP].flags))
    print("Got packet " + str(ip[TCP].flags) + " source ip : %s dest: %s" % ((ip.src), (ip.dst)))
    
    print("SEQ " + str(ip.getlayer(TCP).seq) +" ACK " + str(ip.getlayer(TCP).ack) + " TCP seg len: " + str(get_tcp_seg_len(ip)))
    
    '''
    ans_ack,unans_ack = sr(IP(dst=ip)/TCP(sport=pkt[1].dport, \
                                        dport=pkt[1].sport, \
                                        seq=ip[1].ack, \
                                        ack=ip[1].seq + tcp_seg_len, \
                                        flags="A"), \
                            verbose=0, timeout=1)
    '''
    if not ip.haslayer('Raw'):
        pkt.accept()
        return

    if ip.haslayer(TCP) and ip[TCP].payload is not '':
        #print("Got a packet ! source ip : %s dest: %s" % (str(ip.src), str(ip.dst)))
        #print("Got a packet ! source ip : %s dest: %s" % ((ip.src), (ip.dst)))
        print("Content: ", bytes(ip.getlayer(TCP).payload))
        #tcp_payload = bytes(ip[TCP].payload).replace(b'Message',b'MessagE')
        
        #tcp_payload = bytes(ip[TCP].payload).replace(b'Message',b'MSG12')
        #tcp_payload = bytes(ip[TCP].payload).replace(b'Message',b'Message+')
        #print(tcp_payload)
        
        #payload_before = len(ip[TCP].payload)
        #payload_after = len(tcp_payload)
        #payload_dif = payload_after - payload_before
        #ip[IP].len = ip[IP].len + payload_dif

        #ip[IP].ttl = 40

        #print('Data: '+ str(ip[TCP].payload))
        #print('TTL: ' + str(ip[IP].ttl))

        # set new tcp content
        #ip[Raw].load = tcp_payload

        #del ip[IP].len
        #del ip[IP].chksum
        #del ip[TCP].chksum
        
        #pkt.set_verdict_modified(NetfilterQueue.NF_ACCEPT, str(ip), len(ip))
        #del ip[TCP].len
        
        pkt.set_payload(ip.__bytes__())
    
    
    # extract modifiable byte array representing tcp content (not header)
    #tcp_payload = [it for it in ip['Raw'].load]

    #print(tcp_payload)

    #del ip['TCP'].len
    #del ip['TCP'].chksum

    # overwrite netfilter packet with modified scapy packet and accept
    #pkt.set_payload(ip.__bytes__())
    
    pkt.accept()

# main - program entry point
def main():
    port = str(65432)
    #cmd = "sudo iptables -A FORWARD -s 169.254.95.71 -d 169.254.217.65 -p tcp --dport "+ port + " -j NFQUEUE --queue-num 1"
    #cmd = "sudo iptables -A FORWARD -s 192.168.1.109 -d 192.168.1.131 -p tcp --dport "+ port + " -j NFQUEUE --queue-num 9"
    cmd = "sudo iptables -I INPUT -s 192.168.0.13 -d 192.168.0.12 -p tcp --dport "+ port + " -j NFQUEUE --queue-num 9"
    cmd = "sudo iptables -I INPUT -d 192.168.0.13 -s 192.168.0.12 -p tcp -j NFQUEUE --queue-num 9"
    #returned_value = os.system(cmd)  # returns the exit code in unix
    #print('Starting iptables nfqueue: ', bool(~returned_value))

    # set up netfilter queue
    nfq = NetfilterQueue()
    nfq.bind(9, callback)
    nfq.bind(10, callback)

    try:
        nfq.run()
    except KeyboardInterrupt:
        pass
    finally:
        nfq.unbind()
        #cmd = "sudo iptables -D FORWARD -s 169.254.95.71 -d 169.254.217.65 -p tcp --dport "+ port + " -j NFQUEUE --queue-num 1"
        #cmd = "sudo iptables -I INPUT -s 192.168.0.13 -d 192.168.0.12 -p tcp --dport "+ port + " -j NFQUEUE --queue-num 9"
        #returned_value = os.system(cmd)  # returns the exit code in unix
        #print('Ending iptables nfqueue: ', bool(~returned_value))

if __name__ == '__main__':
	main()
    