'''
	Developed by Knox Cavitt, Bhaskar Gaur, and John Sadik
	Captures and prints certificates
'''

from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *
from valid_helpers import *

import os
import hexdump
import re
import random

from cryptography import x509
from cryptography.hazmat.backends import default_backend

# Global Vars to maintain state
counter = 0
certificate_data = ['', 0]
client_hello = ''
curr_dns = ''


# Might need to change to hashmap as per Dr.Ruoti
captured_certs = {}
banned_certs = {}


def callback(pkt):
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload

	# Does the querry for the dns packets and sets curr dns
	if ip.haslayer(DNS):
		global curr_dns	
	
		summary = ip["DNS"].mysummary()

		if summary.find("DNS Qry") >= 0:
			curr_dns = summary

	# Checking to see if the packet contains tcp data
	if ip.haslayer("Raw"):
		global counter
		global certificate_data
		global client_hello
		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		

		
		print("{0}: {1}".format(counter, kind_header))	

			
		search = re.search('16 03 01', kind_load)
		if search != None:
			client_hello = kind_load

		# Type version length handshake-type length curve-type named-curve pubkey-length
		search = re.search('16 03 03 .. .. 0b .. .. .. .. .. .. ..', kind_load)
		


		# Regex returns a none type if the pattern isnt found
		if search != None:

			# Grabing the range of the match
			range = search.span()
			print("Found a match at range: {0}".format(search.span()))
			print("The certificate data: {0}".format(kind_load[range[0]:range[1]]))	

			# Printing the size of the key and calculate the length as chars
			cert_size_hex_string = kind_load[range[0]+9 : range[0]+14]	
			print("Cert key size: hex-{0} decimal-{1}".format(cert_size_hex_string, int(cert_size_hex_string.replace(' ', ''), 16)))
			cert_size_chars = int(cert_size_hex_string.replace(' ', ''), 16) * 3	
			

			# Inititlizing the certificate data			
			certificate_data[1] = cert_size_chars - len(kind_load[range[0]+15:]) 
			certificate_data[0] += kind_load[range[0]+15:]
		
		elif int(certificate_data[1]) > int(0):


			# If I have enough room in my remaining chars to add the whole of the packet then I do
			if len(kind_load) <= int(certificate_data[1]):
				certificate_data[0] += kind_load
				certificate_data[1] -= len(kind_load)
			else:
				certificate_data[0] += kind_load[:int(certificate_data[1])]
				certificate_data[1] = 0

				# Stores the hex string in a file
				crt = certificate_data[0][30:-2]
				fout = open('cert.txt', 'w')
				fout.write(crt)
				fout.close()
				

				# Converts hex string to a DER and a PEM
				os.system('xxd -r -ps cert.txt cert.der; touch cert.pem ;openssl x509 -in cert.der -inform der -out cert.pem')
					
				# Loads in the PEM using the cryptography library
				fin = open('cert.pem', 'rb')
				loaded_cert = x509.load_pem_x509_certificate(fin.read(), default_backend())
				fin.close()
				
				certificate_data = ['', 0]		
		
				####Certificate Validations###

				valid = [0, 0, 0]
				global captured_certs

				
				if is_banned(loaded_cert, banned_certs) == 1:
					print("Banned Cert!!!")
					pkt.drop()
					return

				
				if was_captured(loaded_cert, captured_certs) != 1: 


					valid[0] = valid_date(loaded_cert)	
					valid[1] = valid_subject(loaded_cert, client_hello)
					valid[2] = valid_subject_dns(loaded_cert, curr_dns)
					valid[1] = 0	
					if len(valid) != sum(valid):
						print("INVALID!")
						add_to_banned(loaded_cert, banned_certs)
						

						if ip.haslayer(TCP):
							print('I have a layer and I\'m bad')
							
							import time
					
							t = [0 ]
							for i in t:
								#test_reset_client = IP(src=ip[IP].src, dst=ip[IP].dst) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=seq_num)	
								seq_num = 0	
								seq_num = random.randint(max(-seq_num, -(ip[TCP].seq)), seq_num)
								seq_num = seq_num + ip[TCP].ack #bgaur - need to add the ack to avoid sending pckts with 0 seq num
								test_reset = IP(src=ip[IP].dst, dst=ip[IP].src) / TCP(sport=ip[TCP].dport, dport=ip[TCP].sport, flags="R", seq=seq_num)		
								send(test_reset, iface='wlan0')
								#send(test_reset_client, iface='eth0')
								time.sleep(.5)

					else:
						print("VALID!")	
						
	
					add_to_captured(loaded_cert, captured_certs)
				
					dump_cert(loaded_cert)
				else:
					print("Already Validated!!!")

				

			#print(ip[IP].src)	


	pkt.accept()


def main():
	net_filter = nfq()

	#os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	#os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --sport 443 -j NFQUEUE --queue-num 3 --queue-bypass')
	#os.system('sudo iptables -I FORWARD -p tcp --dport 443 -j NFQUEUE --queue-num 4 --queue-bypass')
	os.system('sudo iptables -I OUTPUT -j NFQUEUE --queue-num 5 --queue-bypass')	
	os.system('sudo iptables -I INPUT -j NFQUEUE --queue-num 6 --queue-bypass')
	
	#net_filter.bind(1, callback)
	#net_filter.bind(2, callback)	
	#net_filter.bind(3, callback)
	#net_filter.bind(4, callback)
	net_filter.bind(5, callback)
	net_filter.bind(6, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()	

	return


if __name__ == "__main__":
	main()
