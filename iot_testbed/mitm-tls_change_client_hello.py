'''
	Developed by Knox Cavitt and Bhaskar Gaur
	Capture tcp packets with loads that begin with 16 03 03 00

'''
from netfilterqueue import NetfilterQueue as nfq
from scapy.all import *

import os
import hexdump
import re
import time

counter = 0
block = 2

def blank_key(num_words: int) -> str:
	ret = ''

	for i in range(0, num_words):
		ret += '00 '

	return ret[:-1]

def change_client_cipher(kind_load: str, range: Tuple[int, int]):
	#the cipher suit length is 44th Byte in kind load
	cipher_size_loc = 44
	x = range[0] + cipher_size_loc*3

	#convert the length in hex to int
	cs_len = kind_load[x:x+5].replace(' ','')
	cs_len = int(cs_len,16)

	print("The cipher spec length is: {0}".format(cs_len))
	
	x = x + 2*3
	cipher_suites_loc = [46, (46+cs_len*3)]
	cipher_suites = kind_load[x: (x+cs_len*3)]
	print("The cipher suites are: {0}".format(cipher_suites))

	insecure_suites = "c0 2c c0 2c 00 9f cc a9 cc a8 cc aa c0 2b c0 2f 00 9e c0 24 c0 28 00 6b c0 23 c0 27 00 67 c0 0a c0 14 00 39 c0 09 c0 13 00 33 00 9d 00 9c 00 3d 00 3c 00 35 00 2f 00 ff"

	#change all the ciphers to unsecure ciphers	
	alt_load = kind_load[:x] + insecure_suites + kind_load[x+cs_len*3:]
	#alt_load = kind_load[:x] + cipher_suites + kind_load[x+cs_len*3:]
	print(kind_load)
	#print("###############################")
	#alt_load.replace('\n','')
	#print(alt_load[690:710])

	return alt_load

def callback(pkt):
	
	payload = pkt.get_payload() # Gets the payload from the nfq packet
	ip = IP(payload) # Creates a scapy packet at the IP level from the captured payload
	
	# Checking to see if the packet contains tcp data
	if ip.haslayer("Raw"):
		global counter
		counter += 1
		raw_load = ip["Raw"].load # Grab the raw load
		kind_load = ' '.join('{:02x}'.format(x) for x in raw_load) # Make is friendly
		kind_header = kind_load[:18] 
		kind_init = kind_load[:2]		

		
		print("{0}: {1}".format(counter, kind_header))	

		#Client hello signature
		search = re.search('16 03 01 .. .. 01', kind_load)
		
		# Regex returns a none type if the pattern isnt found
		if search != None:
			global block
			block -= 1
			print(block)

			# Grabing the range of the match
			range = search.span()
			print("Found a match at range: {0}".format(search.span()))

			#alt_load = change_server_common_name(kind_load, range)
			alt_load = change_client_cipher(kind_load, range)
			#alt_load = kind_load
			print("The client data: {0}".format(kind_load[range[0]:range[1]]))

			#alt_load_array = alt_load.split(' ')
			alt_load_bytes = bytes.fromhex(alt_load)
			ip['Raw'].load = alt_load_bytes

			# modify length in ip header
			# delete checksums to force scapy to recalculate them
			del ip['IP'].chksum
			del ip['TCP'].chksum

			# overwrite netfilter packet with modified scapy packet and accept
			pkt.set_payload(ip.__bytes__())
			
	pkt.accept()

def main():
	net_filter = nfq()
	os.system('sudo iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1 --queue-bypass')
	os.system('sudo iptables -I INPUT -p tcp --sport 443 -j NFQUEUE --queue-num 2 --queue-bypass')

	net_filter.bind(1, callback)
	net_filter.bind(2, callback)
	
	try:
		net_filter.run()
	except KeyboardInterrupt:
		pass
	finally:	
		net_filter.unbind()
		os.system('sudo iptables -F')

	return

if __name__ == "__main__":
	main()