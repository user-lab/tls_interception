#!/usr/bin/python3

from netfilterqueue import NetfilterQueue
from scapy.all import *
import hexdump


# iptables -I OUTPUT -p tcp --dport 443 -j NFQUEUE --queue-num 1
# ./mitm-tls_downgrade.py
# echo | openssl s_client -connect ocw.cs.pub.ro:443
# iptables - 	tl	 D OUTPUT 1


# callback - called when a new packet is to be evaluated
#	@pkt - netfilterqueue Packet (accept / drop / ...)
def callback(pkt):
	
	load_layer('tls')
	# initialize a scapy ip packet with received data
	payload = pkt.get_payload()
	ip      = IP(payload)
	
	
	if ip.haslayer("Raw"):	
		ip.show()
		tcp_payload = [it for it in ip['Raw'].load]
		print("#############################################")
		print(' '.join('{:02x}'.format(x) for x in tcp_payload))

	pkt.accept()


# main - program entry point
def main():
	# set up netfilter queue
	nfq = NetfilterQueue()
	nfq.bind(1, callback)
	nfq.bind(2, callback)

	try:
		nfq.run()
	except KeyboardInterrupt:
		pass
	finally:
		nfq.unbind()

if __name__ == '__main__':
	main()